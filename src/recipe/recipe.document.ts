import { Document, Types } from 'mongoose';
import { IItem, ItemDocument } from '../item/item.document';
import ObjectId = Types.ObjectId;

export interface RecipeItem extends IItem {
  _id?: ObjectId;
  quantity: number;
}

export enum Profession {
  ALCHEMY = 'Alchemy',
  BLACKSMITHING = 'Blacksmithing',
  COOKING = 'Cooking',
  ENCHANTING = 'Enchanting',
  ENGINNEERING = 'Engineering',
  HERBALISM = 'Herbalism',
  FIRST_AID = 'First Aid',
  FISHING = 'Fishing',
  MINING = 'Mining',
  SKINNING = 'Skinning',
  TAILORING = 'Tailoring',
  LEATHERWORKING = 'Leatherworking'
}

export interface IRecipe {
  profession: Profession;
  components: RecipeItem[];
  createdAt: Date;
  public: boolean;
  createdBy: ObjectId;
  item: ItemDocument;
  itemResult: ItemDocument;
}

export interface RecipeDocument extends IRecipe, Document {}
