import * as express from 'express';
import { Validator } from 'express-json-validator-middleware';
import { ItemController } from './item.controller';
import { RouterManager } from '../utils/router.manager';
import { internalAuthenticationMiddleware } from '../middlewares/internal-authentication.middleware';
import { internalAuthorizationMiddleware } from '../middlewares/internal-authorization.middleware';
import { ItemCreateSchema, ItemQuerySchema, ItemUpdateSchema } from './item.schemas';

const router: express.Router = express.Router();
const validator = new Validator({ allErrors: true, removeAdditional: true });
const itemController = new ItemController();

const routerManager = new RouterManager(router);
const resource = 'items';

/**
 * @apiDefine Item Item
 *
 * List of endpoints for managing items.
 */
routerManager
  .route('/items')
  /**
   * @api {get} /items Get item list
   *
   * @apiGroup Item
   *
   * @apiSuccess {array} items
   *
   * @apiSuccessExample {json} Success response
   *     HTTP/1.1 200 OK
   *     [{
   *        "_id": "5b179f629fea4000ffcf2fbc",
   *        "resource": "user",
   *        "action":["find"]
   *    }]
   */
  .get({
    handlers: [internalAuthenticationMiddleware, internalAuthorizationMiddleware, itemController.getAll],
    resource,
    action: 'read'
  })
  /**
   * @api {post} /items Create item
   *
   * @apiGroup Item
   *
   * @apiParam {String} name,
   *
   * @apiSuccess {String} name
   * @apiSuccess {String} id ObjectId
   *
   * @apiSuccessExample {json} Success response
   *     HTTP/1.1 201 Created
   *     {
   *        "_id": "5b179f629fea4000ffcf2fbc",
   *        "resource": "name"
   *    }
   */
  .post({
    handlers: [
      validator.validate({
        body: ItemCreateSchema
      }),
      internalAuthenticationMiddleware,
      internalAuthorizationMiddleware,
      itemController.create
    ],
    resource,
    action: 'create'
  });

routerManager.route('/items/search').get({
  handlers: [internalAuthenticationMiddleware, internalAuthorizationMiddleware, itemController.search],
  resource,
  action: 'read'
});
routerManager
  .route('/items/:id')
  /**
   * @api {get} /items/:id Get item
   *
   * @apiGroup Item
   *
   * @apiParam {String} id ObjectId
   *
   * @apiSuccess {String} name
   * @apiSuccess {String} id ObjectId
   *
   * @apiSuccessExample {json} Success response
   *     HTTP/1.1 200 OK
   *     {
   *        "_id": "5b179f629fea4000ffcf2fbc",
   *        "resource": "user",
   *        "action":["find"]
   *    }
   *
   * @apiErrorExample {json} Error item not found
   *     HTTP/1.1 404 Not Found
   *     {
   *       "error": {
   *          "code": "ERRNOTFOUND",
   *          "message": "Not found"
   *       }
   *     }
   */
  .get({
    handlers: [
      validator.validate({
        params: ItemQuerySchema
      }),
      internalAuthenticationMiddleware,
      internalAuthorizationMiddleware,
      itemController.get
    ],
    resource,
    action: 'read'
  })
  .put({
    handlers: [
      validator.validate({
        body: ItemUpdateSchema
      }),
      internalAuthenticationMiddleware,
      internalAuthorizationMiddleware,
      itemController.update
    ],
    resource,
    action: 'update'
  });

export default router;
