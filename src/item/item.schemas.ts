export const ItemCreateSchema = {
  type: 'object',
  required: ['name'],
  additionalProperties: false,
  properties: {
    name: {
      type: 'string',
      unique: true
    },
    fixedPrice: {
      type: 'number'
    }
  }
};

export const ItemQuerySchema = {
  type: 'object',
  required: ['id'],
  additionalProperties: false,
  properties: {
    id: {
      type: 'string',
      pattern: '^[0-9a-fA-F]{24}$'
    }
  }
};

export const ItemUpdateSchema = {
  type: 'object',
  required: ['fixedPrice'],
  additionalProperties: false,
  properties: {
    fixedPrice: {
      type: 'number'
    }
  }
};
